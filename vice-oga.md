# VICE for Odroid Go Advance

![vice_in_oga][vice_in_oga]

Instructions to compile VICE (c64 emulator) using [Odroid Go Advance][oga]'s [official image].

[vice_in_oga]: https://lh3.googleusercontent.com/XZonOy89HfiJFXSfkKZhDgE1Olzz6MrpxQQgwUZFIjDn5nTeaBNwfGIpuZQy8XuwF6cXjWlOgE8wlyGWHs_85WTKAqtJnHJ3s9HSFTEAUrzkCl0-NqlZzPw-0BE5nGQAHJWnvCAB8DI=-no
[oga]: https://www.hardkernel.com/shop/odroid-go-advance/
[official image]: https://wiki.odroid.com/odroid_go_advance/make_sd_card

### Install toolchain

Packages needed to compile the different projects

```
$ sudo apt install git g++ gcc flex bison make autoconf cmake
```

### Stop Emulation Station

Each CPU cycle is needed, just kill what is not needed for compilation.

```
$ systemctl stop emulationstation
```

### Compile and Install librga

[librga] provides an API to blit rotated surfaces directly to video card.

```
$ git clone https://github.com/rockchip-linux/linux-rga.git
$ cd linux-rga
$ mkdir build
$ make CFLAGS=-fPIC PROJECT_DIR=build

# Library
$ sudo cp build/lib/librga.so /usr/local/lib/aarch64-linux-gnu/

# Headers
$ sudo mkdir /usr/local/include/rga/
$ sudo cp drmrga.h /usr/local/include/rga/
$ sudo cp rga.h /usr/local/include/rga/
$ sudo cp RgaApi.h /usr/local/include/rga/
$ sudo cp RockchipRgaMacro.h /usr/local/include/rga/
```

[librga]: https://github.com/rockchip-linux/linux-rga

### Compile and Install libSDL

[SDL][libsdl] is the Simple DirectMedia Layer, used by thousands of games.

Using a patched version that uses `librga`.

1. Install SDL dependencies:

```
$ sudo apt install libx11-dev libsm-dev libxext-dev libudev-dev libdrm-dev libgbm-dev zlib1g-dev pkg-config libasound2-dev libfreetype6-dev libx11-xcb1 libxcb-dri2-0
```

2. And compile it:

```
$ git clone https://github.com/AreaScout/SDL.git
$ cd SDL
$ ./autogen.sh
$ ./configure --disable-video-opengl --enable-video-kmsdrm
$ make -j3
$ sudo make install
```

[libsdl]: https://www.libsdl.org/

### Compile and Install VICE

1. Download VICE 3.4 from here: https://sourceforge.net/projects/vice-emu/files/releases/vice-3.4.tar.gz/download

Newer versions should work as well.

2. Install VICE dependencies:

```
$ sudo apt install texinfo xa65
```

3. Compile VICE:

```
$ tar xf vice-3.4.tar.gz
$ cd vice-3.4
$ ./configure --enable-sdlui2 --enable-x64
$ make -j3
$ sudo make install
```

4. Install the config files:

Create and copy `sdl-vicerc` to `~/.config/vice`:

```
[C64]
MenuKey=293
MenuKeyUp=273
MenuKeyDown=274
MenuKeyLeft=276
MenuKeyRight=275
MenuKeyPageUp=280
MenuKeyPageDown=281
MenuKeyHome=278
MenuKeyEnd=279
SaveResourcesOnExit=1
SoundDeviceName="alsa"
SoundBufferSize=100
KeymapIndex=1
AspectRatio="1.000000"
VICIIVideoCache=1
VICIIDoubleSize=0
VICIIFullscreen=1
SidEngine=0
SidFilters=0
SidModel=1
JoyDevice1=4
Acia1Base=56832
```

Create and copy `sdl-joymap-C64.vjm` to `~/.config/vice`:

```
# VICE joystick mapping file
#
# A joystick map is read in as patch to the current map.
#
# File format:
# - comment lines start with '#'
# - keyword lines start with '!keyword'
# - normal line has 'joynum inputtype inputindex action'
#
# Keywords and their lines are:
# '!CLEAR'    clear all mappings
#
# inputtype:
# 0      axis
# 1      button
# 2      hat
# 3      ball
#
# Note that each axis has 2 inputindex entries and each hat has 4.
#
# action [action_parameters]:
# 0               none
# 1 port pin      joystick (pin: 1/2/4/8/16 = u/d/l/r/fire)
# 2 row col       keyboard
# 3               map
# 4               UI activate
# 5 path&to&item  UI function
#

!CLEAR

# odroidgo2_joypad
0 0 0 1 1 8
0 0 1 1 1 4
0 0 2 1 1 2
0 0 3 1 1 1

0 1 0 1 0 16
0 1 1 4
0 1 2 1 0 1
0 1 3 1 0 16
0 1 4 1 1 16
0 1 5 4
0 1 6 1 0 1
0 1 7 1 0 2
0 1 8 1 0 4
0 1 9 1 0 8
0 1 10 3
0 1 11 1 0 16
0 1 12 1 1 16
0 1 13 4
0 1 14 3
0 1 15 1 0 16
```

Create and copy `sdl-hotkey-C64.vjm` to `~/.config/vice`.

*TODO*: Assign the "F6" button to swap joystick ports

```
# VICE hotkey mapping file
#
# A hotkey map is read in as a patch to the current map.
#
# File format:
# - comment lines start with '#'
# - keyword lines start with '!keyword'
# - normal line has 'keynum path&to&menuitem'
#
# Keywords and their lines are:
# '!CLEAR'    clear all mappings
#
300 Machine settings&Joystick settings&Swap joystick ports
1130 Machine settings&Joystick settings&Swap joystick ports
```

5. Run it:

```
$ x64

# or

$ x64 /path/to/game.d64
```